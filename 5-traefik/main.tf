module "traefik" {
  source      = "../modules/terraform-google-managed-instance-group"
  version     = "1.1.14"
  region      = "${var.region}"
  zone        = "${var.zone}"
  name        = "${var.traefik_group_name}"

  target_tags = [
    "ok-80",
  ]

  network     = "${var.network}"
  subnetwork  = "${var.subnet_pub}"
  startup_script = "${data.template_file.startup_traefik.rendered}"

  compute_image = "${data.google_compute_image.hashistack.self_link}"

  # hc
  http_health_check = false
  hc_path = "${var.traefik_hc_path}"
  service_port = "${var.traefik_hc_port}"
  service_port_name = "${var.traefik_hc_name}"

  update_strategy    = "ROLLING_UPDATE"
  service_account_email = "${var.email_account}"

  metadata = {
    cluster-size = "2"
  }

  zonal = "${var.traefik_zonal}"
  machine_type = "${var.traefik_machine_type}"

  ssh_fw_rule = false
  ssh_source_ranges = ["${var.priv_range}"]

  rolling_update_policy = [{
    type                  = "PROACTIVE"
    minimal_action        = "REPLACE"
    max_surge_fixed       = 4
    max_unavailable_fixed = 4
    min_ready_sec         = 50
  }]

}

data "template_file" "startup_traefik" {
  template = "${file("${format("%s/startup-traefik.sh.tpl", path.module)}")}"

  vars {
    domain = "${var.traefik_domain}"
    cluster_tag_name = "${var.consul_client_cluster_tag_name}"
  }
}
