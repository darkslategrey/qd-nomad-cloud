RAND=$(pwgen -A 4)
SERVICE_ACCOUNT=terraform
IMAGE_PROJECT=images-yj4b
PROJECT_NAME=cloudtest-${RAND}
EMAIL="lucien.farstein@gmail.com"
BILLING_ACCOUNT="0171CD-116F3F-B4C737"

! [ -f /tmp/machines.data ] && \
  gcloud compute instances list | grep RUNNING > /tmp/machines.data

GW=$(cat /tmp/machines.data | grep gateway | awk '{ print $5 }')
CONSUL_ADMIN=$(cat /tmp/machines.data | grep server | awk '{ print "8500:"$4":8500" }')
NOMAD_ADMIN=$(cat /tmp/machines.data | grep server | awk '{ print "4646:"$4":4646" }')
TRAEFIK_ADMIN=$(cat /tmp/machines.data | grep traefik | awk '{ print "8080:"$4":8080" }')
