output "pub_link" {
  value = "${google_compute_subnetwork.pub.self_link}"
}
output "priv_link" {
  value = "${google_compute_subnetwork.priv.self_link}"
}
output "net_link" {
  value = "${google_compute_network.nomad.self_link}"
}
