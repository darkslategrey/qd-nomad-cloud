resource "google_compute_network" "nomad" {
  name = "${var.network}"
  project = "${var.project}"
  auto_create_subnetworks = "false"
}

resource "google_compute_subnetwork" "pub" {
  name          = "${var.subnet_pub}"
  ip_cidr_range = "${var.pub_range}"
  network       = "${google_compute_network.nomad.self_link}"
  depends_on    = ["google_compute_network.nomad"]
}

resource "google_compute_subnetwork" "priv" {
  name          = "${var.subnet_priv}"
  ip_cidr_range = "${var.priv_range}"
  network       = "${google_compute_network.nomad.self_link}"
  depends_on    = ["google_compute_network.nomad"]
}
