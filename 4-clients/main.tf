data "template_file" "startup_client" {
  template = "${file("${format("%s/startup-client.sh.tpl", path.module)}")}"

  vars {
    cluster_tag_name = "${var.consul_client_cluster_tag_name}"
  }
}

module "consul_clients" {
  source = "../modules/terraform-google-consul/modules/consul-cluster"
  gcp_zone = "${var.zone}"
  gcp_region = "${var.region}"

  cluster_name = "${var.consul_client_cluster_name}"
  cluster_description = "Consul Client cluster"
  cluster_size = "${var.consul_client_cluster_size}"
  cluster_tag_name = "${var.consul_client_cluster_tag_name}"
  startup_script = "${data.template_file.startup_client.rendered}"

  allowed_inbound_cidr_blocks_dns = []
  allowed_inbound_tags_dns = []

  machine_type = "${var.consul_client_machine_type}"
  root_volume_disk_type = "${var.consul_client_disk_type}"
  root_volume_disk_size_gb = "${var.consul_client_disk_size}"

  assign_public_ip_addresses = false

  source_image = "${data.google_compute_image.hashistack.self_link}"

  instance_group_update_strategy = "ROLLING_UPDATE"
  custom_tags = ["${data.terraform_remote_state.gateway.tag_regional}"]
  # custom_tags = [
  #   "${module.nat.routing_tag_regional}",
  #   "consul-cluster",
  #   "nomad-cluster"
  # ]

  network_name = "${var.network}"
  subnetwork_name = "${var.subnet_priv}"
  email_account = "${var.email_account}"

  # rolling_update_policy_type =  "${var.rolling_update_policy_type}"
  # rolling_update_policy_minimal_action =  "${var.rolling_update_policy_minimal_action}"
  # rolling_update_policy_max_surge_fixed =  "${var.rolling_update_policy_max_surge_fixed}"
  # # rolling_update_policy_max_surge_percent =  "${var.rolling_update_policy_max_surge_percent}"
  # rolling_update_policy_max_unavailable_fixed =  "${var.rolling_update_policy_max_unavailable_fixed}"
  # # rolling_update_policy_max_unavailable_percent =  "${var.rolling_update_policy_max_unavailable_percent}"
  # rolling_update_policy_min_ready_sec =  "${var.rolling_update_policy_min_ready_sec}"
}

# Render the Startup Script that will run on each Consul Server Instance on boot.
# This script will configure and start Consul.

# resource "google_compute_region_autoscaler" "consul-client-scaler" {
#   name   = "consul-client-autoscaler"
#   region = "europe-west1"
#   target = "${module.consul_clients.instance_group_url}"

#   autoscaling_policy = {
#     max_replicas    = 5
#     min_replicas    = 2
#     cooldown_period = 60

#     cpu_utilization {
#       target = 0.7
#     }
#   }
# }
