
# les machines

#+BEGIN_SRC sh
gcloud compute instances list
#+END_SRC

#+RESULTS:
| NAME                | ZONE           | MACHINE_TYPE |  PREEMPTIBLE |    INTERNAL_IP | EXTERNAL_IP | STATUS |
| bastion             | europe-west1-b | f1-micro     |   172.27.3.2 | 35.205.105.176 | RUNNING     |        |
| consul-cluster-mcjh | europe-west1-b | g1-small     | 172.27.3.130 | 35.241.244.140 | RUNNING     |        |
| instance-1          | europe-west1-b | g1-small     | 172.27.3.131 | 35.189.210.252 | RUNNING     |        |


# utils

ssh into natted instances from gateway

#+BEGIN_SRC shell
gcloud beta compute ssh [INTERNAL_INSTANCE_NAME] --internal-ip
#+END_SRC


tunnel ssh to nomad admin

#+BEGIN_SRC shell
ssh -i ~/.ssh/google_compute_engine -L 4646:172.27.3.130:4646 104.199.107.54
#+END_SRC

tunnel ssh to consul admin
#+BEGIN_SRC shell
ssh -i ~/.ssh/google_compute_engine -L 8500:172.27.3.130:8500 104.199.107.54
#+END_SRC

