variable "project" { default = "cloudtest-nxe0" }
variable "region"  { default = "europe-west1"   }
variable "zone"    { default = "europe-west1-b" }
variable "email_account" {
  default = "terraform@cloudtest-nxe0.iam.gserviceaccount.com"
}

# network
variable "network"     { default = "nomad" }
variable "subnet_priv" { default = "priv" }
variable "subnet_pub"  { default = "pub" }
variable "priv_range"  { default = "172.27.3.128/26" }
variable "pub_range"   { default = "172.27.3.0/26" }

# gateway
variable "nat_name" { default = "bastion" }

# servers
variable "consul_server_cluster_size" { default = "1" }
variable "consul_server_cluster_tag_name" { default = "consul-servers" }
variable "consul_server_cluster_name" { default = "consul-servers" }
variable "consul_server_machine_type" { default = "g1-small" }
variable "consul_server_disk_type"    { default = "pd-standard" }
variable "consul_server_disk_size"    { default = "15" }

# clients
variable "consul_client_cluster_name" { default = "consul-clients" }
variable "consul_client_cluster_size" { default = "1" }
variable "consul_client_cluster_tag_name" { default = "consul-clients" }
variable "consul_client_machine_type" { default = "g1-small" }
variable "consul_client_disk_type"    { default = "pd-standard" }
variable "consul_client_disk_size"    { default = "15" }

# traefik
variable "traefik_group_name" { default = "traefik-ig" }
variable "traefik_hc_port"    { default = "80" }
variable "traefik_hc_name"    { default = "http" }
variable "traefik_hc_path"    { default = "/ping" }
variable "traefik_machine_type" { default = "n1-standard-1" }
variable "traefik_zonal" { default = "false" }
variable "traefik_domain" { default = "zapto.org" }
variable "traefik_network_tag_name" { default = "traefik" }

# images
data "google_compute_image" "hashistack" {
  name = "hashistack-v6"
  # project = "images-yj4b"
}
