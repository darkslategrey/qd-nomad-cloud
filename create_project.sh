
. ./vars.sh

echo "PROJECT_NAME=${PROJECT_NAME}" > .project_name

gcloud projects create ${PROJECT_NAME} && \
gcloud beta billing projects link ${PROJECT_NAME} \
       --billing-account ${BILLING_ACCOUNT}

gcloud config configurations create ${PROJECT_NAME} && \
    gcloud config configurations activate ${PROJECT_NAME} && \
    gcloud config set project ${PROJECT_NAME} && \
    gcloud config set account ${EMAIL}


gcloud iam service-accounts  create ${SERVICE_ACCOUNT} --display-name "${SERVICE_ACCOUNT}" && \
    gcloud iam service-accounts keys create ~/.config/gcloud/${SERVICE_ACCOUNT}.json --iam-account ${SERVICE_ACCOUNT}@${PROJECT_NAME}.iam.gserviceaccount.com

gcloud services enable cloudresourcemanager.googleapis.com && \
gcloud services enable cloudbilling.googleapis.com && \
gcloud services enable iam.googleapis.com && \
gcloud services enable compute.googleapis.com

# TODO: activer resourcemanager sur rrrework
GOOGLE_PROJECT=${IMAGE_PROJECT} gcloud services enable cloudresourcemanager.googleapis.com

# gcloud iam service-accounts get-iam-policy \
#        ${SERVICE_ACCOUNT}@${PROJECT_NAME}.iam.gserviceaccount.com \
#        --format json > policy.json

gcloud projects add-iam-policy-binding ${PROJECT_NAME} \
       --member serviceAccount:${SERVICE_ACCOUNT}@${PROJECT_NAME}.iam.gserviceaccount.com --role roles/compute.admin
gcloud projects add-iam-policy-binding ${PROJECT_NAME} \
       --member serviceAccount:${SERVICE_ACCOUNT}@${PROJECT_NAME}.iam.gserviceaccount.com --role roles/editor
gcloud projects add-iam-policy-binding ${PROJECT_NAME} \
       --member serviceAccount:${SERVICE_ACCOUNT}@${PROJECT_NAME}.iam.gserviceaccount.com --role roles/compute.networkAdmin

       # --role roles/compute.networkAdmin \
       # --role roles/compute.securityAdmin \
       # --role roles/compute.instanceAdmin.v1 \
       # --role roles/compute.admin \
       # --role roles/iam.serviceAccountUser

PROJECT_ID=$(gcloud projects list | grep ${PROJECT_NAME} | awk '{ print $3 }')

gcloud projects add-iam-policy-binding ${IMAGE_PROJECT} \
       --member serviceAccount:${PROJECT_ID}@cloudservices.gserviceaccount.com \
       --role roles/compute.imageUser

# gcloud projects add-iam-policy-binding ${IMAGE_PROJECT} \
#        --member serviceAccount:${SERVICE_ACCOUNT}@${PROJECT_NAME}.iam.gserviceaccount.com \
#        --role roles/compute.imageUser

# gcloud iam service-accounts set-iam-policy \
#        my-sa-123@my-project-123.iam.gserviceaccount.com policy.json

