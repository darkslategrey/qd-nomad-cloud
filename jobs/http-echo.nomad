job "docs" {
  datacenters = ["europe-west1-b", "europe-west1-c", "europe-west1-d"]
  # datacenters = ["dc1"]

  group "example" {
    task "server" {
      driver = "exec"

      template {
        data = "<html><body><h4>hello !</h4></body></html>"
        destination = "index.html"
      }
      config {
        command = "/usr/bin/python"
        args = [
          "-m",
          "SimpleHTTPServer",
          "8000",
        ]
      }

      service {
        name = "echo-http"
        tags = [
          # "traefik.tags=public",
          "traefik.frontend.rule=Host:on.zapto.org",
          "traefik.frontend.entryPoints=http",
          "traefik.tags=exposed"
          # "traefik.frontend.rule=Host: bla.staging.cloud.courseur.com"
        ]
        # PathPrefixStrip:/0/",
        # tags = ["global", "cache"]
        port = "http"

        check {
          name     = "alive"
          type     = "tcp"
          interval = "10s"
          timeout  = "2s"
        }
      }

      resources {
        network {
          mbits = 10
          port "http" {
            static = "8000"
          }
        }
      }
    }
  }
}
