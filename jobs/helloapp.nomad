# There can only be a single job definition per file.
# Create a job with ID and Name 'example'
job "helloapp" {
  datacenters = ["europe-west1-b", "europe-west1-c", "europe-west1-d"]
  # datacenters = ["dc1"]

  constraint {
    attribute = "${attr.kernel.name}"
    value = "linux"
  }

  # Configure the job to do rolling updates
  update {
    stagger = "10s"
    max_parallel = 1
  }

  group "hello" {
    count = 1

    restart {
      attempts = 2
      interval = "1m"
      delay = "10s"
      mode = "fail"
    }

    # Define a task to run
    task "hello" {
      driver = "docker"

      config {
        image = "gerlacdt/helloapp:v0.1.0"
        auth {
          server_address = "hub.docker.com:443"
        }
        port_map {
          http = 8080
        }
      }
      service {
        name = "${TASKGROUP}-service"
        tags = [
          # "traefik.tags=public",
          "traefik.frontend.rule=Host:on.zapto.org",
          "traefik.frontend.entryPoints=http",
          "traefik.tags=exposed"
        ]
        port = "http"
        check {
          name = "alive"
          type = "http"
          interval = "10s"
          timeout = "3s"
          path = "/health"
        }
      }

      resources {
        cpu = 500 # 500 MHz
        memory = 128 # 128MB
        network {
          mbits = 1
          port "http" {
          }
        }
      }

      logs {
        max_files = 10
        max_file_size = 15
      }
      kill_timeout = "10s"
    }
  }
}
