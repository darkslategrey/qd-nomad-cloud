data "template_file" "startup_server" {
  template = "${file("${format("%s/startup-server.sh.tpl", path.module)}")}"

  vars {
    cluster_tag_name = "${var.consul_server_cluster_tag_name}"
    cluster_size     = "${var.consul_server_cluster_size}"
  }
}
module "consul_serveurs" {
  source = "../modules/terraform-google-consul/modules/consul-cluster"
  gcp_zone = "${var.zone}"
  gcp_region = "${var.region}"
  cluster_name = "${var.consul_server_cluster_name}"
  cluster_description = "Consul Server cluster"
  cluster_size = "${var.consul_server_cluster_size}"
  cluster_tag_name = "${var.consul_server_cluster_tag_name}"
  startup_script = "${data.template_file.startup_server.rendered}"

  custom_tags = ["${data.terraform_remote_state.gateway.tag_regional}"]

  machine_type             = "${var.consul_server_machine_type}"
  root_volume_disk_type    = "${var.consul_server_disk_type}"
  root_volume_disk_size_gb = "${var.consul_server_disk_size}"

  source_image = "${data.google_compute_image.hashistack.self_link}"

  assign_public_ip_addresses = false
  instance_group_update_strategy = "ROLLING_UPDATE"

  email_account = "${var.email_account}"
  network_name = "${var.network}"
  subnetwork_name = "${var.subnet_priv}"

  # rolling_update_policy = [{
  #   type                  = "PROACTIVE"
  #   minimal_action        = "REPLACE"
  #   max_surge_fixed       = 4
  #   max_unavailable_fixed = 4
  #   min_ready_sec         = 50
  # }]
}
