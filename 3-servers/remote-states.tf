data "terraform_remote_state" "gateway" {
  backend = "local"

  config {
    path = "../2-gateway/terraform.tfstate"
  }
}
