module "nat" {

  source     = "git::https://gitlab.com/darkslategrey/terraform-google-nat-gateway.git?ref=v1.3"
  # source     = "GoogleCloudPlatform/nat-gateway/google"
  
  region     = "${var.region}"
  name       = "${var.nat_name}"
  network    = "${var.network}"
  # compute_image = "${data.google_compute_image.hashistack.self_link}"
  subnetwork = "${var.subnet_priv}"
  tags       = [
    "${var.consul_server_cluster_tag_name }",
    "${var.consul_client_cluster_tag_name }"
  ]
}
