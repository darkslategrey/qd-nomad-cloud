# resource "google_compute_firewall" "nomad-servers" {
#   name    = "nomad-servers"
#   network = "${var.network_name}"

#   allow {
#     protocol = "tcp"
#     ports    = ["4646-4648"]
#   }

#   allow {
#     protocol = "udp"
#     ports    = ["4648"]
#   }

#   # source_tags   = ["bastion"]
#   # source_ranges = ["0.0.0.0/0"]
#   source_tags = ["inst-bastionnat-europe-west1", "nomad-cluster", "consul-cluster"]
#   target_tags   = ["nomad-servers", "nomad-cluster", "consul-cluster", "inst-bastionnat-europe-west1"]
#   # target_tags   = ["bastionnat-europe-west1"]
#   # target_tags   = ["nomad-cluster"]
# }

# resource "google_compute_firewall" "nomad-clients" {
#   name    = "nomad-clients"
#   network = "${var.network_name}"

#   allow {
#     protocol = "tcp"
#     ports    = ["4646-4647", "3000"]
#   }

#   source_tags   = ["consul-cluster", "bastion", "nomad-cluster"]
#   # source_tags   = ["inst-bastionnat-europe-west1"]
#   # target_tags   = ["bastionnat-europe-west1", "nomad-cluster"]
#   target_tags   = ["nomad-clients", "consul-clients", "nomad-cluster", "consul-cluster", "inst-bastionnat-europe-west1"]
# }

# resource "google_compute_firewall" "nomad-apps" {
#   name    = "nomad-apps"
#   network = "${var.network_name}"

#   allow {
#     protocol = "tcp"
#     ports    = ["4000-60000", "80", "443"]
#   }

#   allow {
#     protocol = "udp"
#     ports    = ["4000-60000"]
#   }

#   source_tags   = ["traefik", "nomad-clients", "consul-cluster", "nomad-cluster", "inst-bastionnat-europe-west1"]
#   # source_tags   = ["inst-bastionnat-europe-west1"]
#   # target_tags   = ["bastionnat-europe-west1", "nomad-cluster"]
#   target_tags   = ["nomad-clients", "consul-clients", "nomad-cluster", "consul-cluster", "inst-bastionnat-europe-west1"]
# }
