# module "traefiks" {
#   source = "./terraform-google-consul/modules/consul-cluster"

#   gcp_zone = "${var.gcp_zone}"
#   gcp_region = "${var.gcp_region}"

#   cluster_name = "${var.consul_client_cluster_name}"
#   cluster_description = "Traefik cluster"
#   cluster_size = "${var.traefik_cluster_size}"
#   cluster_tag_name = "${var.consul_client_cluster_tag_name}"
#   startup_script = "${data.template_file.startup_traefik.rendered}"

#   allowed_inbound_cidr_blocks_http_api = []
#   allowed_inbound_tags_http_api = []

#   allowed_inbound_cidr_blocks_dns = []
#   allowed_inbound_tags_dns = []

#   machine_type = "g1-small"
#   root_volume_disk_type = "pd-standard"
#   root_volume_disk_size_gb = "15"

#   assign_public_ip_addresses = true

#   # source_image = "${var.consul_client_source_image}"
#   source_image = "${data.google_compute_image.hashistack.self_link}" 
#   # Our Consul Clients are completely stateless, so we are free to destroy and re-create them as needed.
#   instance_group_update_strategy = "${var.instance_group_update_strategy}"

#   network_name = "${var.network_name}"
#   # subnetwork_name = "${var.subnetwork_name}"
#   subnetwork_name = "pub"
#   email_account = "${var.email_account}"

#   custom_tags = ["consul-cluster", "traefik"]
# }

# data "template_file" "startup_traefik" {
#   template = "${file("${format("%s/startup-traefik.sh.tpl", path.module)}")}"

#   vars {
#     domain = "${var.domain}"
#     cluster_tag_name = "${var.cluster_tag_name}"
#   }
# }
