resource "google_compute_firewall" "cluster-ssh" {
  name    = "cluster-ssh"
  network = "${var.network_name}"

  allow {
    protocol = "tcp"
    ports = ["22"]
  }

  source_tags = ["inst-bastionnat-europe-west1", "consul-cluster", "nomad-cluster"]
  source_tags = ["inst-bastionnat-europe-west1", "consul-cluster", "nomad-cluster"]
  # source_ranges = ["0.0.0.0/0"]
  # target_tags = ["inst-bastionnat-europe-west1"]
}

# resource "google_compute_firewall" "consul-servers" {
#   name    = "consul-servers"
#   network = "${var.network_name}"

#   allow {
#     protocol = "tcp"
#     ports    = ["8300-8302", "8500"]
#   }

#   allow {
#     protocol = "udp"
#     ports    = ["8301-8302"]
#   }

#   # source_tags   = ["inst-bastionnat-europe-west1"]
#   target_tags   = ["nomad-cluster"]
#   source_tags   = ["traefik", "nomad-cluster", "inst-bastionnat-europe-west1"]
#   # source_ranges = ["0.0.0.0/0"]
#   # target_tags   = ["bastionnat-europe-west1"]
# }

# resource "google_compute_firewall" "consul-clients" {
#   name    = "consul-clients"
#   network = "${var.network_name}"

#   allow {
#     protocol = "tcp"
#     ports    = ["8300-8301", "8500"]
#   }

#   allow {
#     protocol = "udp"
#     ports    = ["8301"]
#   }

#   # source_tags   = ["inst-bastionnat-europe-west1"]
#   target_tags   = ["traefik", "nomad-cluster", "inst-bastionnat-europe-west1"]
#   source_tags   = ["traefik", "nomad-cluster", "inst-bastionnat-europe-west1"]
#   # source_ranges = ["0.0.0.0/0"]
#   # target_tags   = ["nomad-cluster", "inst-bastionnat-europe-west1"]
# }
