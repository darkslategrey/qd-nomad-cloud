resource "google_compute_firewall" "dns" {
  name    = "dns"
  network = "${var.network_name}"

  allow {
    protocol = "tcp"
    ports    = ["53"]
  }

  allow {
    protocol = "udp"
    ports    = ["53"]
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "icmp" {
  name    = "icmp"
  network = "${var.network_name}"

  allow {
    protocol = "icmp"
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "nomad_ssh" {
  name    = "nomad-ssh"
  network = "${var.network_name}"

  allow {
    protocol = "tcp"
    ports = ["22"]
  }

  # source_tags = ["allow-ssh", "inst-bastionnat-europe-west1", "consul-cluster"]
  source_ranges = ["0.0.0.0/0"]
  target_tags = ["inst-bastionnat-europe-west1"]
}

# resource "google_compute_firewall" "no-consul-adm-1" {
#   name    = "no-consul-adm-2"
#   network = "${var.network_name}"

#   deny {
#     protocol = "tcp"
#     ports = ["8500", "8300"]
#   }

#   # source_tags = ["inst-bastionnat-europe-west1", "consul-cluster"]
#   source_ranges = ["0.0.0.0/0"]
#   target_tags = ["consul-cluster", "inst-bastionnat-europe-west1"]
# }

# resource "google_compute_firewall" "no-consul-adm-2" {
#   name    = "no-consul-adm-1"
#   network = "${var.network_name}"

#   allow {
#     protocol = "tcp"
#     ports = ["8500", "8300"]
#   }

#   source_tags = ["inst-bastionnat-europe-west1", "consul-cluster"]
#   # source_ranges = ["0.0.0.0/0"]
#   target_tags = ["consul-cluster", "inst-bastionnat-europe-west1"]
# }

