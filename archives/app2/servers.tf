data "template_file" "startup_server" {
  template = "${file("${format("%s/startup-server.sh.tpl", path.module)}")}"

  vars {
    cluster_tag_name = "${var.consul_server_cluster_tag_name}"
    cluster_size     = "${var.consul_server_cluster_size}"
  }
}

module "nat" {
  source     = "GoogleCloudPlatform/nat-gateway/google"
  region     = "europe-west1"
  name       = "bastion"
  network    = "${var.network_name}"
  compute_image = "${data.google_compute_image.hashistack.self_link}"
  subnetwork = "priv"
  tags       = ["${var.consul_server_cluster_tag_name }"]
}

module "consul_serveurs" {
  # source = "git::ssh://
  source = "./terraform-google-consul/modules/consul-cluster"
  gcp_zone = "${var.gcp_zone}"
  gcp_region = "${var.gcp_region}"
  cluster_name = "${var.consul_server_cluster_name}"
  cluster_description = "Consul Server cluster"
  cluster_size = "${var.consul_server_cluster_size}"
  cluster_tag_name = "${var.consul_server_cluster_tag_name}"
  startup_script = "${data.template_file.startup_server.rendered}"

  allowed_inbound_tags_http_api = ["${var.consul_client_cluster_tag_name}", "inst-bastionnat-europe-west1"]
  allowed_inbound_tags_dns = ["${var.consul_client_cluster_tag_name }"]

  custom_tags = ["${module.nat.routing_tag_regional}"]

  machine_type = "g1-small"
  root_volume_disk_type = "pd-standard"
  root_volume_disk_size_gb = "15"

  source_image = "${data.google_compute_image.hashistack.self_link}"

  assign_public_ip_addresses = false
  instance_group_update_strategy = "ROLLING_UPDATE"

  email_account = "${var.email_account}"
  network_name = "${var.network_name}"
  subnetwork_name = "${var.subnetwork_name}"

  # rolling_update_policy = [{
  #   type                  = "PROACTIVE"
  #   minimal_action        = "REPLACE"
  #   max_surge_fixed       = 4
  #   max_unavailable_fixed = 4
  #   min_ready_sec         = 50
  # }]

  # update_strategy    = "ROLLING_UPDATE"
}

# module "mig" {
#   source      = "GoogleCloudPlatform/managed-instance-group/google"
#   version     = "1.1.14"
#   region      = "europe-west1"
#   zone        = "europe-west1-b"
#   name        = "nomad-ig"
#   target_tags = ["${module.nat.routing_tag_regional}", "nomad-cluster"]
#   network     = "${var.network}"
#   subnetwork  = "priv"
#   startup_script = "${data.template_file.startup-script.rendered}"
#   access_config = []
#   # compute_image = "image-yj4b/projects/hashistack-v5"
#   compute_image = "${data.google_compute_image.hashistack.self_link}"
#   service_port = "80"
#   service_port_name = "http"
#   update_strategy    = "ROLLING_UPDATE"
#   service_account_email = "terraform@cloudtest-nxe0.iam.gserviceaccount.com"

#   # TOOD: add cluster-size = 2 to custom meta
#   metadata = {
#     cluster-size = "2"
#   }
  
#   zonal = false
#   machine_type = "n1-standard-1"
#   rolling_update_policy = [{
#     type                  = "PROACTIVE"
#     minimal_action        = "REPLACE"
#     max_surge_fixed       = 4
#     max_unavailable_fixed = 4
#     min_ready_sec         = 50
#   }]

# }
