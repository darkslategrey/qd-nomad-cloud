#!/bin/bash -xe

apt-get update
apt-get install -y nginx-light

# install lookbusy script to test autoscaling
# apt-get install -y build-essential git
# cd /tmp
# git clone https://github.com/beloglazov/cpu-load-generator.git
# ./cpu-load-generator/install-lookbusy.sh
# cd -

systemctl enable nginx
systemctl restart nginx
