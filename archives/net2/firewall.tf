resource "google_compute_firewall" "icmp" {
  name    = "icmp"
  network = "${var.network}"

  allow {
    protocol = "icmp"
  }

  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "consul-servers" {
  name    = "consul-servers"
  network = "${var.network}"

  allow {
    protocol = "tcp"
    ports    = ["8300-8302", "8500"]
  }

  allow {
    protocol = "udp"
    ports    = ["8301-8302"]
  }

  # source_tags   = ["inst-bastionnat-europe-west1"]
  target_tags   = ["nomad-cluster"]
  source_ranges = ["0.0.0.0/0"]
  # target_tags   = ["bastionnat-europe-west1"]
}

resource "google_compute_firewall" "consul-clients" {
  name    = "consul-clients"
  network = "${var.network}"

  allow {
    protocol = "tcp"
    ports    = ["8300-8301", "8500"]
  }

  allow {
    protocol = "udp"
    ports    = ["8301"]
  }

  # source_tags   = ["inst-bastionnat-europe-west1"]
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["bastionnat-europe-west1", "nomad-cluster"]
}
