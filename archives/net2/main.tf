# resource "google_compute_network" "nomad" {
#   name = "nomad"
#   project = "${var.project}"
#   auto_create_subnetworks = "false"
# }

# resource "google_compute_subnetwork" "priv" {
#   name          = "priv"
#   project       = "${var.project}"
#   ip_cidr_range = "172.27.3.128/26"
#   region        = "europe-west1"

#   network       = "${google_compute_network.nomad.self_link}"
#   depends_on = ["google_compute_network.nomad"]
# }
data "google_compute_image" "hashistack" {
  name = "hashistack-v5"
  project = "images-yj4b"
}
data "template_file" "startup-script" {
  template = "${file("${format("%s/startup.sh.tpl", path.module)}")}"

  vars {
    # PROXY_PATH = ""
  }
}

module "nat" {
  source     = "GoogleCloudPlatform/nat-gateway/google"
  region     = "europe-west1"
  name       = "bastion"
  network    = "${var.network}"
  subnetwork = "priv"
  tags       = ["bastion"]
}

module "mig" {
  source      = "GoogleCloudPlatform/managed-instance-group/google"
  version     = "1.1.14"
  region      = "europe-west1"
  zone        = "europe-west1-b"
  name        = "nomad-ig"
  target_tags = ["${module.nat.routing_tag_regional}", "nomad-cluster"]
  network     = "${var.network}"
  subnetwork  = "priv"
  startup_script = "${data.template_file.startup-script.rendered}"
  access_config = []
  # compute_image = "image-yj4b/projects/hashistack-v5"
  compute_image = "${data.google_compute_image.hashistack.self_link}"
  service_port = "80"
  service_port_name = "http"
  update_strategy    = "ROLLING_UPDATE"
  service_account_email = "terraform@cloudtest-nxe0.iam.gserviceaccount.com"

  # TOOD: add cluster-size = 2 to custom meta
  metadata = {
    cluster-size = "2"
  }
  
  zonal = false
  machine_type = "n1-standard-1"
  rolling_update_policy = [{
    type                  = "PROACTIVE"
    minimal_action        = "REPLACE"
    max_surge_fixed       = 4
    max_unavailable_fixed = 4
    min_ready_sec         = 50
  }]

}
