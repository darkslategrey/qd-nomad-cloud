variable "network" { default = "nomad" }
variable "project" { default = "cloudtest-nxe0" }
provider "google" {
  region = "europe-west1"
  project = "${var.project}"
}
