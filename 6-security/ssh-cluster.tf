resource "google_compute_firewall" "cluster-ssh" {
  name    = "cluster-ssh"
  network = "${var.network}"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }

  source_tags = ["inst-${data.terraform_remote_state.gateway.tag_regional}"]
}
