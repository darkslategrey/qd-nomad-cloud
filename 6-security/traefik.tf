resource "google_compute_firewall" "public-traefik" {
  name    = "public-traefik"
  network = "${var.network}"

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  priority = 900
  # source_tags = ["inst-${data.terraform_remote_state.gateway.tag_regional}"]
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["ok-80"]
  count = 1
}

resource "google_compute_firewall" "traefik-adm" {
  name    = "traefik-adm"
  network = "${var.network}"

  allow {
    protocol = "tcp"
    ports    = ["8080"]
  }

  source_tags = ["inst-${data.terraform_remote_state.gateway.tag_regional}"]
  target_tags   = ["ok-80"]
}

resource "google_compute_firewall" "consul-traefik" {
  name    = "consul-traefik"
  network = "${var.network}"

  allow {
    protocol = "tcp"
    ports    = ["8300-8301", "8500"]
  }

  allow {
    protocol = "udp"
    ports    = ["8301"]
  }

  source_tags   = ["ok-80"]
  target_tags   = ["consul-servers", "consul-clients"]
}
