resource "google_compute_firewall" "nomad-servers" {
  name    = "nomad-servers"
  network = "${var.network}"

  allow {
    protocol = "tcp"
    ports    = ["4646-4648"]
  }

  allow {
    protocol = "udp"
    ports    = ["4648"]
  }

  source_tags = [
    "inst-${data.terraform_remote_state.gateway.tag_regional}",
    "${var.consul_server_cluster_tag_name}",
    "${var.consul_client_cluster_tag_name}"
  ]

  target_tags = [
    "${var.consul_server_cluster_tag_name}",
    "${var.consul_client_cluster_tag_name}"
  ]
}

# resource "google_compute_firewall" "nomad-clients" {
#   name    = "nomad-clients"
#   network = "${var.network}"

#   allow {
#     protocol = "tcp"
#     ports    = ["4646-4647"]
#   }

#   source_tags   = [ "${var.consul_server_cluster_tag_name}" ]
#   target_tags   = [ "${var.consul_client_cluster_tag_name}" ]
# }

resource "google_compute_firewall" "nomad-apps" {
  name    = "nomad-apps"
  network = "${var.network}"

  allow {
    protocol = "tcp"
    ports    = ["1000-9999", "20000-60000", "80", "443"]
  }

  allow {
    protocol = "udp"
    ports    = ["1000-9999", "20000-60000"]
  }

  source_tags = [ "ok-80", "${var.consul_client_cluster_tag_name}" ]
  target_tags = [ "${var.consul_client_cluster_tag_name}" ]
}
