# export GOPATH=$HOME/src/go
export GOOGLE_APPLICATION_CREDENTIALS=$HOME/.config/gcloud/terraform.json

. ./.project_name
# export GOOGLE_PROJECT=$(cat .project_name)

export GOOGLE_CLOUD_PROJECT=${GOOGLE_PROJECT}
export GCLOUD_PROJECT=${GOOGLE_PROJECT}
export CLOUDSDK_CORE_PROJECT=${GOOGLE_PROJECT}

export GOOGLE_REGION="europe-west1"
export GCLOUD_REGION=${GOOGLE_REGION}
export CLOUDSDK_COMPUTE_REGION=${GOOGLE_REGION}

export GOOGLE_ZONE="europe-west1-b"
export GCLOUD_ZONE=${GOOGLE_ZONE}
export CLOUDSDK_COMPUTE_ZONE=${GOOGLE_ZONE}


terraform apply
# terraform destroy
